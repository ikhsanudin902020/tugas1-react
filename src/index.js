import React, { createElement } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import image from "./logo.svg";
import * as serviceWorker from "./serviceWorker";

const element_jsx = <h2> Belajar Menggunakan JSX</h2>;
const element_react = createElement("h1", {}, "Tugas 1 - React");
const ekspresi_js = (
  <div>
    <h1>Elemen pertama dengan h1</h1>
    <h3>Elemen kedua dengan h3</h3>
    <p> Elemen ketiga dengan p</p>
  </div>
);
const elemenAtribut_ClassName = (
  <h1 className="warna-merah">
    Tulisan ini akan berwarna merah karena menggunakan atribut className dengan
    nilai warna-merah (styling di index.css)
  </h1>
);

const elemenAtribut_src = <img src={image} style={{width:500}}/>


const elemen_child = (
  <div>
    <h1>Elemen dengan child 1</h1>
    <h3>Elemen dengan child 2</h3>
    <p> Elemen dengan child 3</p>
  </div>
);

const elemen_tanpaChild = <img src={image} style={{width:500}}/>

//elemen dengan child
//ReactDOM.render(elemen_child, document.getElementById("root"));

//elemen tanpa child
ReactDOM.render(elemen_tanpaChild, document.getElementById("root"));


serviceWorker.unregister();

//elemen dengan atribut
// ReactDOM.render(elemenAtribut_ClassName, document.getElementById("root"));

//elemen dengan atribut
//ReactDOM.render(elemenAtribut_src, document.getElementById("root"));

//elemen dengan jsx
// ReactDOM.render(
//   elemen_jsx,
//   document.getElementById('root')
// );

//elemen dengan fungsi createElement()
// ReactDOM.render(
//   element_react,
//   document.getElementById('root')
// );

//elemen dengan ekspresi
// ReactDOM.render(
//   ekspresi_js,
//   document.getElementById('root')
// );

serviceWorker.unregister();
